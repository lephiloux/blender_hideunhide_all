import bpy 

bl_info = {
    "name": "Completly Hide from both display port and render all child of a selected parent object",
    "blender": (3, 3, 0),
    "author": "Philippe Van Hecke",
    "category": "Object",
}

class HideUnHideBase(bpy.types.Operator):
    
    _viewport = False
    _renderer = False
    _set = False
    def recursiveSelect(self,parent):
      for child in parent.children:
        child.select_set(True)
        if child.children:
            recursiveSelect(child)

    def execute(self,context):     
      initialSelection = bpy.context.selected_objects
      for obj in initialSelection:
          self.recursiveSelect(obj)
    
      for obj in bpy.context.selected_objects:
          if self._viewport:
             obj.hide_set( self._set  )
          if self._renderer:
             obj.hide_render = self._set  
          
      return {'FINISHED'}
  
    
class HideViewPortSelectedAndChildrens(HideUnHideBase):
    """ Completly Hide from both display port and render all child of a selected parent object """
    bl_idname ="object.hide_viewport_all"
    bl_label = "Hide All Childrens View Port"
    bl_options = {'REGISTER','UNDO'}

    def __init__(self):
          self._viewport = True
          self._set = True
        
    
  
class HideRendererSelectedAndChildrens(HideUnHideBase):
    """ Completly Hide from both display port and render all child of a selected parent object """
    bl_idname ="object.hide_render_all"
    bl_label = "Hide All Childrens Renderer"
    bl_options = {'REGISTER','UNDO'}

    def __init__(self):
          self._renderer = True  
          self._set = True

  


class UnHideViewPortSelectedAndChildrens(HideUnHideBase):
    """ Completly Hide from both display port and render all child of a selected parent object """
    bl_idname ="object.unhide_viewport_all"
    bl_label = "UnHide All Childrens View Port"
    bl_options = {'REGISTER','UNDO'}

    def __init__(self):
          self._viewport = True
          self._set = False
        
    
  
class UnHideRendererSelectedAndChildrens(HideUnHideBase):
    """ Completly Hide from both display port and render all child of a selected parent object """
    bl_idname ="object.unhide_render_all"
    bl_label = "UnHide All Childrens Renderer"
    bl_options = {'REGISTER','UNDO'}

    def __init__(self):
          self._renderer = True
          self._set = False

  
  
  
def menu_func(self,context):
        self.layout.operator(HideViewPortSelectedAndChildrens.bl_idname)
        self.layout.operator(HideRendererSelectedAndChildrens.bl_idname)
        self.layout.operator(UnHideViewPortSelectedAndChildrens.bl_idname)
        self.layout.operator(UnHideRendererSelectedAndChildrens.bl_idname)
        
# store keymaps here to access after registration
addon_keymaps = []        

def register():
    bpy.utils.register_class(HideViewPortSelectedAndChildrens)
    bpy.utils.register_class(HideRendererSelectedAndChildrens)
    bpy.utils.register_class(UnHideViewPortSelectedAndChildrens)
    bpy.utils.register_class(UnHideRendererSelectedAndChildrens)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc:
        km = wm.keyconfigs.addon.keymaps.new(name='Object Mode', space_type='EMPTY')
        kmi = km.keymap_items.new(HideViewPortSelectedAndChildrens.bl_idname, 'H', 'PRESS', ctrl=True)
        addon_keymaps.append((km,kmi))
        kmi = km.keymap_items.new(HideRendererSelectedAndChildrens.bl_idname, 'H', 'PRESS', ctrl=True, shift=True)
        addon_keymaps.append((km,kmi))
        kmi = km.keymap_items.new(UnHideViewPortSelectedAndChildrens.bl_idname, 'H', 'PRESS', ctrl=True, alt=True)
        addon_keymaps.append((km,kmi))
        kmi = km.keymap_items.new(UnHideRendererSelectedAndChildrens.bl_idname, 'H', 'PRESS', shift=True, alt=True)
        addon_keymaps.append((km,kmi))
            
#        kmi.properties.total = 4
    
    bpy.types.VIEW3D_MT_object.append(menu_func)
    
def unregister():
    for km,kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    bpy.types.VIEW3D_MT_object.remove(menu_func)
    
    bpy.utils.unregister_class(HideViewPortSelectedAndChildrens)
    bpy.utils.unregister_class(HideRendererSelectedAndChildrens)
    bpy.utils.unregister_class(UnHideViewPortSelectedAndChildrens)
    bpy.utils.unregister_class(UnHideRendererSelectedAndChildrens)            
    
    
if __name__ == "__main__":
  register()
        