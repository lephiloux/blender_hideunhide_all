# Blender Hideunhide All



## Hide / UnHide selected object
This simple addon hide / unhide selected object from view port or renderer and all childrens associated.

Hide object and all childrens in view port is mapped to Ctrl-H
Hide object and all childrens in renderer is mapped to Shift-Crl-H
UnHide object and all childrens in view port is mapped to Ctrl-Alt-H ( but this one not yet working because when hidden is not selected any more)
UnHide object and all childrens in renderer is mapped to Shift-Alt-H
